﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ClienteWF
{
    public partial class LibroForm : Form
    {
        public LibroForm()
        {
            InitializeComponent();
        }

        private void LibroForm_Load(object sender, EventArgs e)
        {
            listar();
        }


        private void btnAgregar_Click(object sender, EventArgs e)
        {
            RegistroLibroForm form = new RegistroLibroForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                listar();
            }

        }

        private void dgvLibro_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string codigo = dgvLibro.Rows[e.RowIndex].Cells[0].Value.ToString();
            RegistroLibroForm form = new RegistroLibroForm(codigo);
            if (form.ShowDialog() == DialogResult.OK)
            {
                listar();
            }
        }

        public void listar()
        {
            List<LibroView> lista = new List<LibroView>();
            try
            {
                LibroService.LibroServiceSoapClient service = new LibroService.LibroServiceSoapClient();
                LibroService.Libro[] listaWS = service.ListarLibros();

                foreach(LibroService.Libro item in listaWS)
                {
                    lista.Add(new LibroView()
                    {
                        codigoLibro = item.codigoLibro,
                        nombreLibro = item.nombreLibro,
                        existencia = item.existencia,
                        precio = item.precio,
                        autor = item.autor.nombre,
                        editorial = item.editorial.nombre,
                        genero = item.genero.nombre,
                        descripcion = item.descripcion
                    });
                }

            }catch (Exception ex)
            {
                lista = new List<LibroView>();
            }
            dgvLibro.DataSource = lista;
        }

        
    }
}
