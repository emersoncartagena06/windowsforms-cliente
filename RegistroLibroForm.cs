﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClienteWF
{
    public partial class RegistroLibroForm : Form
    {

        private string codigoLibro { get; set; }

        public RegistroLibroForm()
        {
            InitializeComponent();
        }

        public RegistroLibroForm(string codigoLibro)
        {
            InitializeComponent();
            this.codigoLibro = codigoLibro;
        }

        private void RegistroLibroForm_Load(object sender, EventArgs e)
        {

            listarAutores();
            listarEditoriales();
            listarGeneros();

            if (this.codigoLibro == null)
            {
                this.Text = "Agregar Libro";
                btnEliminar.Visible = false;
            }
            else
            {
                this.Text = "Editar Libro";
                txCodigo.Text = this.codigoLibro;
                txCodigo.ReadOnly = true;
                buscarLibro();
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string codigo = txCodigo.Text;
                string nombre = txNombre.Text;
                int existencia = int.Parse(txExistencia.Value.ToString());
                decimal precio = decimal.Parse(txPrecio.Text);
                string codigoAutor = cbAutor.SelectedValue.ToString();
                string codigoEditorial = cbEditorial.SelectedValue.ToString();
                int idGenero = int.Parse(cbGenero.SelectedValue.ToString());
                string descripcion = txDescripcion.Text;

                LibroService.LibroServiceSoapClient service = new LibroService.LibroServiceSoapClient();
                bool response = false;

                if(this.codigoLibro == null)
                {
                    response = service.AgregarLibro(codigo, nombre, existencia, precio, codigoAutor, codigoEditorial, idGenero, descripcion);
                }
                else
                {
                    response = service.ModificarLibro(codigo, nombre, existencia, precio, codigoAutor, codigoEditorial, idGenero, descripcion);
                }

                if(response)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Hubo un error");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Hubo un error");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public void listarAutores()
        {
            try
            {
                AutorService.AutorServiceSoapClient service = new AutorService.AutorServiceSoapClient();
                AutorService.Item[] listaWS = service.ListarCombo();

                cbAutor.DataSource = listaWS;
                cbAutor.DisplayMember = "nombre";
                cbAutor.ValueMember = "codigo";

            }catch (Exception ex)
            {
                MessageBox.Show("No se pudo cargar los autores");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        public void listarEditoriales()
        {
            try
            {
                EditorialService.EditorialServiceSoapClient service = new EditorialService.EditorialServiceSoapClient();
                EditorialService.Item[] listaWS = service.ListarCombo();

                cbEditorial.DataSource = listaWS;
                cbEditorial.DisplayMember = "nombre";
                cbEditorial.ValueMember = "codigo";

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo cargar las editoriales");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        public void listarGeneros()
        {
            try
            {
                GeneroService.GeneroServiceSoapClient service = new GeneroService.GeneroServiceSoapClient();
                GeneroService.Item[] listaWS = service.ListarCombo();

                cbGenero.DataSource = listaWS;
                cbGenero.DisplayMember = "nombre";
                cbGenero.ValueMember = "codigo";

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo cargar los generos");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        public void buscarLibro()
        {
            try
            {
                LibroService.LibroServiceSoapClient service = new LibroService.LibroServiceSoapClient();
                LibroService.Libro libroWS = service.BuscarLibro(this.codigoLibro);
                if(libroWS != null)
                {
                    txNombre.Text = libroWS.nombreLibro;
                    txExistencia.Value = libroWS.existencia;
                    txPrecio.Text = libroWS.precio.ToString();
                    cbAutor.SelectedValue = libroWS.autor.codigo;
                    cbEditorial.SelectedValue = libroWS.editorial.codigo;
                    cbGenero.SelectedValue = libroWS.genero.id.ToString();
                    txDescripcion.Text = libroWS.descripcion;
                }
                else
                {
                    MessageBox.Show("No se pudo encontrar el libro");
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("No se pudo encontrar el libro");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

    }
}
