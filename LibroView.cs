﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteWF
{
    public class LibroView
    {
        public string codigoLibro { get; set; }
        public string nombreLibro { get; set; }
        public int existencia { get; set; }
        public decimal precio { get; set; }
        public string autor { get; set; }
        public string editorial { get; set; }
        public string genero { get; set; }
        public string descripcion { get; set; }
    }
}
